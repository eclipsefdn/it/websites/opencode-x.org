---
title: Open Code Experience
seo_title: Open Code Experience 2024 | Oct 22-24
date: 2023-09-26T10:00:00-04:00
headline: ' '
jumbotron_class: col-xs-24
custom_jumbotron_class: col-xs-24
custom_jumbotron: |
  <div class="dark row uppercase padding-y-60">
    <div class="col-sm-12">
      <img class="featured-jumbotron-logo img img-responsive" src="/public/images/ocx-logo.png" alt="OCX 24">
      <h1 class="featured-jumbotron-headline">
        Open Code Experience
      </h1>
    </div>
    <div class="col-sm-10 col-sm-offset-2">
      <p class="featured-jumbotron-custom-tagline">
        Save the date<br>
        October 22-24, 2024<br>
        Mainz, Germany
      </p>
    </div>
  </div>
header_wrapper_class: header-home
hide_breadcrumb: true
hide_sidebar: true
hide_page_title: true
show_featured_footer: false
container: container-fluid
page_css_file: /public/css/home.css
---

{{< grid/section-container class="featured-section bg-secondary dark text-center" >}}
  <h2 class="margin-bottom-20" id="about-ocx">About OCX</h2> 
  <p class="margin-bottom-30">
    As the Eclipse Foundation strives to inspire our vibrant community of
    communities, we have expanded our annual conference to create Open Code
    Experience (OCX). This new developer conference will host collocated events
    such as <strong>EclipseCon 2024</strong>, the centre of gravity for all topics related to
    the Eclipse IDE platform and developer tools; <strong>Open Code for Java</strong> focusing
    on topics related to Jakarta EE, Adoptium, MicroProfile, and more; and <strong>Open
    Code for Automotive</strong> dedicated to automotive software, software-defined
    vehicles, and mobility topics.
  </p>
  <a class="btn btn-primary btn-huge" href="https://blogs.eclipse.org/post/thabang-mashologu/unveiling-open-code-experience">
    Learn More
  </a>
{{</ grid/section-container >}}

{{< grid/section-container class="featured-section bg-primary text-black text-center" >}}
  <h2 class="margin-bottom-20" id="become-a-sponsor">Become a Sponsor</h2>
  <p class="margin-bottom-30">
    A new conference means new opportunities to connect with the open source
    community. Open Code Experience (OCX) offers sponsorship packages that will
    help you increase your brand recognition and awareness, demonstrate your
    leadership within the Eclipse Foundation's community, and drive interest in
    the open source projects hosted at the Eclipse Foundation that are
    important to your business.
  </p>
  <a class="btn btn-secondary btn-huge" href="/documents/ocx-sponsor-prospectus-2024.pdf">Become a Sponsor</a>
{{</ grid/section-container >}}
